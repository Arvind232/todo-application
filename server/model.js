const mongoose = require("mongoose");

var schema = new mongoose.Schema({
  task: {
    type: String,
    required: true,
  },
  time: {
    type: String,
    required: true,
  },
  duration: String,
  status: String,
});

const Taskdb = mongoose.model("taskdb", schema);

module.exports = Taskdb;
